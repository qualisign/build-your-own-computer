import itertools
import inspect
import pytest
import re
from byoc.computer.logic._and import _and

def test_allowed():
    text=inspect.getsource(_and)
    assert not (bool(re.search("\\sand", text)))
    
@pytest.mark.parametrize('arg1, arg2',
   # get cartesian product of t/f pairings and feed it into test                       
   itertools.product([True, False], [True, False]) 
)

def test_and(arg1, arg2):
    assert _and(arg1, arg2) == (arg1 and arg2)
