import itertools
import inspect
import pytest
import re
from byoc.computer.logic.xor import xor

@pytest.mark.parametrize('arg1, arg2',
   # get cartesian product of t/f pairings and feed it into test                       
   itertools.product([True, False], [True, False]) 
)

def test_and(arg1, arg2):
    assert(xor(i, j) == ((not (i and j)) and (i or j)))    
