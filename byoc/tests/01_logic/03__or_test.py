import itertools
import inspect
import pytest
import re
from byoc.computer.logic._or import _or

def test_allowed():
    text=inspect.getsource(_or)
    assert not (bool(re.search("\\sor", text)))
    
@pytest.mark.parametrize('arg1, arg2',
   # get cartesian product of t/f pairings and feed it into test                       
   itertools.product([True, False], [True, False]) 
)

def test_or(arg1, arg2):
    assert(_or(arg1, arg2) == (arg1 or arg2))
