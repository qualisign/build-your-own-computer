from byoc.solutions.arithmetic.half_adder import add

def test_sum_bit():
    assert(add(0, 0)[1]==False)
    assert(add(0, 1)[1]==True)
    assert(add(1, 0)[1]==True)
    assert(add(1, 1)[1]==False)

def test_carry_bit():
    assert(add(0, 0)[0]==False)
    assert(add(0, 1)[0]==False)
    assert(add(1, 0)[0]==False)    
    assert(add(1, 1)[0]==True)
