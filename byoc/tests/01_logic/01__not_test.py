import inspect
import pytest
import re
import os
from byoc.computer.logic._not import _not
"""
this is the documentation
"""

def test_allowed():
    text=inspect.getsource(_not)
    assert not (bool(re.search("\\snot", text)))
    
def test_not():    
    assert (_not(True)==(not True)) and (_not(False)==(not False))


    
