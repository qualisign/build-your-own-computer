
IMPLEMENT A NOT GATE

- Create a file called `_not.py` containing a definition for a function called `_not`.  This function should behave the same as the standard `not` operator that comes built in with Python, but it should be callable using postfix notation.

| notation      | expression| 
|---------------|-----------|
| infix         | a not b   |
|---------------|-----------|
| postfix       | _not(a, b)|
|---------------------------|

- You may not use the word "not" anywhere in your code.

- Here's an example:

 _not(True) # ==> False

---------------------------------------------------

- If you get stuck, run:

byoc hint

- When you're done, run:

byoc test

- Have fun!

---------------------------------------------------

For useful background info on logic see:

https://textbooks.open.tudelft.nl/index.php/textbooks/catalog/view/13/14/147-1#page=16
