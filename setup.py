from setuptools import setup, find_packages

setup(
    name='byoc',
    packages=find_packages(),
    scripts=['bin/byoc']
)
