def xor(a, b):
    if a:
        return not b
    return b
