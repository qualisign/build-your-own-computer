
IMPLEMENT AN OR GATE

- Create a file called `_or.py` containing a definition for a function called `_or`.  This function should behave the same as the standard `or` operator that comes built in with Python, but it should be callable using postfix notation.

| notation      | expression| 
|---------------|-----------|
| infix         | a or b    |
|---------------|-----------|
| postfix       | _or(a, b) |
|---------------------------|

- You may not use the word "or" anywhere in your code.

- Here's an example:

 _or(True, False) # ==> True

---------------------------------------------------

- If you get stuck, run:

byoc hint

- When you're done, run:

byoc test

- Have fun!
