# Build Your Own Computer

### Introduction

In this interactive tutorial, you will learn to build your own virtual computer using Python.  

Instead of using physical components like transistors, circuits, and microchips, you will simulate these things programmatically.  

### Target Audience

Anyone hoping to learn more about how computers work on the inside is encouraged to give this tutorial a try.  It should be accessible to anyone with a small amount of Python knowledge.  

__Additional software testing challenges will be added in the near future.__

### Project Status

| MODULE                | STATUS | REFERENCES                     |
|-----------------------|--------|--------------------------------|
| LOGIC                 | ✓      |  PETZOLD Ch. 11,12 <br>NISAN Ch. 1 |
| ARITHMETIC            | -      |  PETZOLD Ch. 12,13 <br>NISAN Ch. 2 |
| MEMORY                | ☓      |  PETZOLD Ch. 14 <br>NISAN Ch. 3    |
| MACHINE LANGUAGE      | ☓      | NISAN Ch. 4 <br>PETZOLD Ch. 15,16  |
| COMPUTER ARCHITECTURE | ☓      | NISAN Ch. 5                    |
| ASSEMBLER             | ☓      |                                |
| VIRTUAL MACHINE       | ☓      |                                |
| HIGH-LEVEL LANGUAGE   | ☓      |                                |
| COMPILER              | ☓      |                                |
| OPERATING SYSTEM      | ☓      |                                |

##### REFERENCES
Petzold - [Code: The Hidden Language of Computer Hardware and Software](https://www.amazon.com/Code-Language-Computer-Developer-Practices-ebook/dp/B00JDMPOK2/ref=sr_1_1?dchild=1&keywords=code&qid=1586868065&s=books&sr=1-1)

Nisan - [The Elements of Computing Systems](https://www.amazon.com/Elements-Computing-Systems-Building-Principles-ebook/dp/B004HHORGA/ref=sr_1_1?dchild=1&keywords=elements+of+computing+systems&qid=1586868035&s=books&sr=1-1)

Feynman - [Feynman Lectures on Computation](https://www.amazon.com/Feynman-Lectures-Computation-Frontiers-Physics/dp/0738202967)

HTTW - [How These Things Work](https://reasonablypolymorphic.com/book)

### Prerequisites
- git
- Python 3
- pytest

### Setup
Just run the following in the terminal:
``` bash  

    git clone https://gitlab.com/qualisign/build-your-own-computer  
 
    pip3 install -e ./build-your-own-computer
```  
If there were no errors, you should be able to run the following commands:

### Commands
#### show list of commands

``` bash
byoc
```
#### show current challenge
``` bash
byoc current
```
#### test your solution
``` bash
byoc test
```
#### show official solution
``` bash
byoc solution
```
#### show hint
``` bash
byoc hint
```

### Contribute

I am very busy and can't imagine being done with this project very soon.  Any help is appreciated.  Just send a pull request!  
