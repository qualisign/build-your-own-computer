
IMPLEMENT AN XOR GATE

- Create a file called `xor.py` containing a definition for a function called `xor`.  
- This function should take two arguments and return True if and only if one of the arguments is True.

- Here's an example:

 xor(True, False) # ==> True

---------------------------------------------------

- If you get stuck, run:

byoc hint

- When you're done, run:

byoc test

- Have fun!
