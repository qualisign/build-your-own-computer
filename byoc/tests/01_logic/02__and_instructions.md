
IMPLEMENT AN AND GATE

- Create a file called `_and.py` containing a definition for a function called `_and`.  This function should behave the same as the standard `and` operator that comes built in with Python, but it should be callable using postfix notation.

| notation      | expression| 
|---------------|-----------|
| infix         | a and b   |
|---------------|-----------|
| postfix       | _and(a, b)|
|---------------------------|

- You may not use the word "and" anywhere in your code.

- Here's an example:

 _and(True, False) # ==> False

---------------------------------------------------

- If you get stuck, run:

byoc hint

- When you're done, run:

byoc test

- Have fun!
